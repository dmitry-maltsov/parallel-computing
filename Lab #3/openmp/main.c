#include <stdio.h>
#include <malloc.h>
#include <time.h>
#include <omp.h>
#include <math.h>

#define THREAD_NUM 4

const double DELTA_T = 0.2;
const int MAX_ITER = 100;
const double K = 60.0;

typedef struct IterationsBound
{
    int startIteration;
    int endIteration;
} IterationsBounds;

typedef struct ImageWithDistancesData
{
    int rowNumber;
    int colNumber;
    int maxGrayInImage;
    int **imageIntensity;
    int **tmpImageIntensity;
} ImageWithDistancesData;

int ** allocateIntMemory(int row, int col)
{
    int * arrayDataMemory = (int *)malloc(row * col * sizeof(int));
    int ** returnedArray = (int **) malloc(row * sizeof(int *));

    for (int i = 0; i < row; i++)
    {
        returnedArray[i] = &(arrayDataMemory[col * i]);
    }

    return returnedArray;
}

void freeIntArray(int **array)
{
    free(array[0]);
    free(array);
}

ImageWithDistancesData* readImageData(const char* fileName, ImageWithDistancesData* data)
{
    FILE* pgmImage;
    char pgmVersion[3];

    pgmImage = fopen(fileName, "rb");

    if (pgmImage == NULL)
    {
        printf("Can't open file");
        exit(1);
    }

    fgets(pgmVersion, sizeof(pgmVersion), pgmImage);

    fscanf(pgmImage, "%d", &(data->colNumber));
    fscanf(pgmImage, "%d", &(data->rowNumber));
    fscanf(pgmImage, "%d", &(data->maxGrayInImage));

    data->imageIntensity = allocateIntMemory(data->rowNumber, data->colNumber);
    data->tmpImageIntensity = allocateIntMemory(data->rowNumber, data->colNumber);

    for (int i = 0; i < data->rowNumber; i++)
    {
        for (int j = 0; j < data->colNumber; j++)
        {
            fscanf(pgmImage, "%d", &((data->imageIntensity)[i][j]));
        }
    }

    fclose(pgmImage);

    return data;
}

int getMaxFor2DArray(const int ** values, int rowSize, int colSize)
{
    int maxValue = 0;

    for (int i = 1; i < rowSize; i++)
    {
        for (int j = 1; j < colSize; j++)
        {
            if (values[i][j] > maxValue)
            {
                maxValue = values[i][j];
            }
        }
    }

    return maxValue;
}

void saveResult(ImageWithDistancesData* data)
{
    FILE* resImg;
    resImg = fopen("result.pgm", "wb");

    fprintf(resImg, "P2\n");
    fprintf(resImg, "%d %d\n", data->colNumber, data->rowNumber);
    fprintf(resImg, "%d\n", data->maxGrayInImage);

    int maxDistance = getMaxFor2DArray((const int **) data->imageIntensity, data->rowNumber, data->colNumber);

    float greyScale = (float) data->maxGrayInImage / (float) maxDistance;

    for (int i = 0; i < data->rowNumber; i++)
    {
        for (int j = 0; j < data->colNumber; j++)
        {
            fprintf(resImg, "%d ", (int) ((float) data->imageIntensity[i][j] * greyScale));
        }
        fprintf(resImg, "\n");
    }

    fclose(resImg);
}

double gComputation(int value)
{
    return exp(-(value/K)*(value/K));
}

IterationsBounds getBounds(ImageWithDistancesData* data)
{
    unsigned int parallelBlockSize = (unsigned int) (data->rowNumber / omp_get_num_threads());

    IterationsBounds iterationsBounds;
    iterationsBounds.startIteration = parallelBlockSize * omp_get_thread_num();
    iterationsBounds.endIteration = iterationsBounds.startIteration + parallelBlockSize - 1;

    if (omp_get_thread_num() + 1 == omp_get_num_threads())
    {
        iterationsBounds.endIteration = data->rowNumber - 1;
    }

    return iterationsBounds;
}

void calculateNextFunc(ImageWithDistancesData *data, int row, int col)
{
    int IN, IS, IE, IW;
    int nablN, nablS, nablE, nablW;
    double CNIComponent, CSIComponent, CEIComponent, CWIComponent;
    IterationsBounds iterationsBounds = getBounds(data);

    for (int i = iterationsBounds.startIteration; i < iterationsBounds.endIteration + 1; ++i)
    {
        for (int j = 0; j < col; ++j)
        {
            int currIntensity = data->imageIntensity[i][j];

            IN = i != 0         ? data->imageIntensity[i - 1][j] : 0;
            IS = i != (row - 1) ? data->imageIntensity[i + 1][j] : 0;
            IE = j != 0         ? data->imageIntensity[i][j - 1] : 0;
            IW = j != (col - 1) ? data->imageIntensity[i][j + 1] : 0;

            nablN = IN - currIntensity;
            nablS = IS - currIntensity;
            nablE = IE - currIntensity;
            nablW = IW - currIntensity;

            CNIComponent = gComputation(abs(nablN)) * (float ) nablN;
            CSIComponent = gComputation(abs(nablS)) * (float ) nablS;
            CEIComponent = gComputation(abs(nablE)) * (float ) nablE;
            CWIComponent = gComputation(abs(nablW)) * (float ) nablW;

            data->tmpImageIntensity[i][j] = currIntensity + (int) (DELTA_T * (CNIComponent + CSIComponent + CWIComponent + CEIComponent));
        }
    }
}

void computeSmoothing(ImageWithDistancesData *data)
{
    int currIter = 0;

    while (currIter < MAX_ITER)
    {
#pragma omp parallel num_threads(THREAD_NUM)
        {
            int row, col;
#pragma omp critical
            {
                row = data->rowNumber;
                col = data->colNumber;
            }

            calculateNextFunc(data, row, col);
        }

        int ** temp = data->tmpImageIntensity;
        data->tmpImageIntensity = data->imageIntensity;
        data->imageIntensity = temp;

        currIter++;
    }
}

void run(const char* filename)
{
    ImageWithDistancesData * data = (ImageWithDistancesData*)malloc(sizeof(ImageWithDistancesData));

    data = readImageData(filename, data);
    computeSmoothing(data);

    saveResult(data);

    freeIntArray(data->imageIntensity);
    freeIntArray(data->tmpImageIntensity);
    free(data);
}

int main(int argc, char* argv[])
{
    const char* fileName = "C:/Users/dmitr/CLionProjects/lab3omp/apollonian.pgm";

    clock_t begin = clock();

    run(fileName);

    clock_t end = clock();

    printf("time: %f", (double)(end - begin) / CLOCKS_PER_SEC);

    return 0;
}
