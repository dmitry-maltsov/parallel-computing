#include <stdio.h>
#include <malloc.h>
#include <mpi.h>
#include <time.h>
#include <math.h>

#define SEND_PREV 4
#define RECEIVE_NEXT 4
#define IMG_DATA 0
#define ROW_NUM 1
#define COL_NUM 2
#define SEND_NEXT 5
#define RECEIVE_PREV 5
#define FINAL 7

const double DELTA_T = 0.2;
const int MAX_ITER = 100;
const double K = 20.0;

MPI_Status status;

typedef struct IterationsBound
{
    int startIteration;
    int endIteration;
} IterationsBounds;

typedef struct ImageWithDistancesData
{
    int rowNumber;
    int colNumber;
    int maxGrayInImage;
    int **imageIntensity;
    int **tmpImageIntensity;
} ImageWithDistancesData;

int ** allocateIntMemory(int row, int col)
{
    int * arrayDataMemory = (int *)malloc(row * col * sizeof(int));
    int ** returnedArray = (int **) malloc(row * sizeof(int *));

    for (int i = 0; i < row; i++)
    {
        returnedArray[i] = &(arrayDataMemory[col * i]);
    }

    return returnedArray;
}

void freeIntArray(int **array)
{
    free(array[0]);
    free(array);
}

ImageWithDistancesData* readImageData(const char* fileName, ImageWithDistancesData* data)
{
    FILE* pgmImage;
    char pgmVersion[3];

    pgmImage = fopen(fileName, "rb");

    if (pgmImage == NULL)
    {
        printf("Can't open file");
        exit(1);
    }

    fgets(pgmVersion, sizeof(pgmVersion), pgmImage);

    fscanf(pgmImage, "%d", &(data->colNumber));
    fscanf(pgmImage, "%d", &(data->rowNumber));
    fscanf(pgmImage, "%d", &(data->maxGrayInImage));

    data->imageIntensity = allocateIntMemory(data->rowNumber, data->colNumber);
    data->tmpImageIntensity = allocateIntMemory(data->rowNumber, data->colNumber);

    for (int i = 0; i < data->rowNumber; i++)
    {
        for (int j = 0; j < data->colNumber; j++)
        {
            fscanf(pgmImage, "%d", &((data->imageIntensity)[i][j]));
        }
    }

    fclose(pgmImage);
    return data;
}

int getMaxFor2DArray(const int ** values, int rowSize, int colSize)
{
    int maxValue = 0;

    for (int i = 1; i < rowSize; i++)
    {
        for (int j = 1; j < colSize; j++)
        {
            if (values[i][j] > maxValue)
            {
                maxValue = values[i][j];
            }
        }
    }

    return (int) maxValue;
}

void saveResult(ImageWithDistancesData* data)
{
    FILE* resImg;
    resImg = fopen("result.pgm", "wb");

    fprintf(resImg, "P2\n");
    fprintf(resImg, "%d %d\n", data->colNumber, data->rowNumber);
    fprintf(resImg, "%d\n", data->maxGrayInImage);

    int maxDistance = getMaxFor2DArray((const int **) data->imageIntensity, data->rowNumber, data->colNumber);

    float greyScale = (float) data->maxGrayInImage / (float) maxDistance;

    for (int i = 0; i < data->rowNumber; i++)
    {
        for (int j = 0; j < data->colNumber; j++)
        {
            fprintf(resImg, "%d ", (int) ((float) data->imageIntensity[i][j] * greyScale));
        }
        fprintf(resImg, "\n");
    }

    fclose(resImg);
}

double gComputation(int value)
{
    return exp(-(value/K)*(value/K));
}

IterationsBounds getBounds(ImageWithDistancesData* data, unsigned int mpiProcessId, unsigned int processesNumber)
{
    unsigned int parallelBlockSize = (unsigned int) (data->rowNumber / processesNumber);

    IterationsBounds iterationsBounds;
    iterationsBounds.startIteration = parallelBlockSize * mpiProcessId;
    iterationsBounds.endIteration = iterationsBounds.startIteration + parallelBlockSize - 1;

    if (mpiProcessId + 1 == processesNumber)
    {
        iterationsBounds.endIteration = data->rowNumber - 1;
    }

    return iterationsBounds;
}

void calculateNextFunc(ImageWithDistancesData *data, IterationsBounds iterationsBounds)
{
    int IN, IS, IE, IW;
    int nablN, nablS, nablE, nablW;
    double CNIComponent, CSIComponent, CEIComponent, CWIComponent;

    for (int i = iterationsBounds.startIteration; i < iterationsBounds.endIteration + 1; ++i)
    {
        for (int j = 0; j < data->colNumber; ++j)
        {
            int currIntensity = data->imageIntensity[i][j];

            IN = i != 0                     ? data->imageIntensity[i - 1][j] : 0;
            IS = i != (data->rowNumber - 1) ? data->imageIntensity[i + 1][j] : 0;
            IE = j != 0                     ? data->imageIntensity[i][j-1] : 0;
            IW = j != (data->colNumber - 1) ? data->imageIntensity[i][j+1] : 0;

            nablN = IN - currIntensity;
            nablS = IS - currIntensity;
            nablE = IE - currIntensity;
            nablW = IW - currIntensity;

            CNIComponent = gComputation(abs(nablN)) * (float ) nablN;
            CSIComponent = gComputation(abs(nablS)) * (float ) nablS;
            CEIComponent = gComputation(abs(nablE)) * (float ) nablE;
            CWIComponent = gComputation(abs(nablW)) * (float ) nablW;

            data->tmpImageIntensity[i][j] = currIntensity + (int) (DELTA_T * (CNIComponent + CSIComponent + CWIComponent + CEIComponent));
        }
    }

    for (int i = 0; i < data->rowNumber; i++)
    {
        for (int j = 0; j < data->colNumber; ++j)
        {
            data->imageIntensity[i][j] = data->tmpImageIntensity[i][j];
        }
    }
}

void computeSmoothing(ImageWithDistancesData *data, int mpiProcessId, int processesNumber)
{
    IterationsBounds iterationsBounds = getBounds(data, mpiProcessId, processesNumber);
    int currIter = 0;

    while (currIter < MAX_ITER) {
        calculateNextFunc(data, iterationsBounds);

        if (mpiProcessId % 2 == 1)
        {
            if (mpiProcessId != 0){
                MPI_Send( &(data->imageIntensity[iterationsBounds.startIteration][0]), data->colNumber, MPI_INT, mpiProcessId - 1, SEND_PREV, MPI_COMM_WORLD );
            }
            if (mpiProcessId + 1 != processesNumber)
            {
                MPI_Send( &(data->imageIntensity[iterationsBounds.endIteration][0]), data->colNumber, MPI_INT, mpiProcessId + 1, SEND_NEXT, MPI_COMM_WORLD );
            }

            if (mpiProcessId + 1 != processesNumber)
            {
                MPI_Recv( &(data->imageIntensity[iterationsBounds.endIteration + 1][0]), data->colNumber, MPI_INT, mpiProcessId + 1, RECEIVE_NEXT, MPI_COMM_WORLD, &status );
            }
            if (mpiProcessId != 0)
            {
                MPI_Recv( &(data->imageIntensity[iterationsBounds.startIteration - 1][0]), data->colNumber, MPI_INT, mpiProcessId - 1, RECEIVE_PREV, MPI_COMM_WORLD, &status );
            }
        } else
        {
            if (mpiProcessId + 1 != processesNumber)
            {
                MPI_Recv( &(data->imageIntensity[iterationsBounds.endIteration + 1][0]), data->colNumber, MPI_INT, mpiProcessId + 1, RECEIVE_NEXT, MPI_COMM_WORLD, &status );
            }
            if (mpiProcessId != 0)
            {
                MPI_Recv( &(data->imageIntensity[iterationsBounds.startIteration - 1][0]), data->colNumber, MPI_INT, mpiProcessId - 1, RECEIVE_PREV, MPI_COMM_WORLD, &status );
            }

            if (mpiProcessId != 0)
            {
                MPI_Send( &(data->imageIntensity[iterationsBounds.startIteration][0]), data->colNumber, MPI_INT, mpiProcessId - 1, SEND_PREV, MPI_COMM_WORLD );
            }
            if (mpiProcessId + 1 != processesNumber)
            {
                MPI_Send( &(data->imageIntensity[iterationsBounds.endIteration][0]), data->colNumber, MPI_INT, mpiProcessId + 1, SEND_NEXT, MPI_COMM_WORLD );
            }
        }
        currIter++;
    }
}

void run(const char* filename, int mpiProcessId, int processesNumber)
{
    ImageWithDistancesData * data = (ImageWithDistancesData*)malloc(sizeof(ImageWithDistancesData));

    if (mpiProcessId == 0)
    {
        data = readImageData(filename, data);

        for (int currProcessNum = 1; currProcessNum < processesNumber; currProcessNum++)
        {
            MPI_Send(&(data->colNumber), 1, MPI_INT, currProcessNum, COL_NUM, MPI_COMM_WORLD );
            MPI_Send(&(data->rowNumber), 1, MPI_INT, currProcessNum, ROW_NUM, MPI_COMM_WORLD );
        }

        for (int currProcessNum = 1; currProcessNum < processesNumber; currProcessNum++)
        {
            MPI_Send(&(data->imageIntensity[0][0]), data->rowNumber * data->colNumber, MPI_INT, currProcessNum, IMG_DATA, MPI_COMM_WORLD );
        }
    } else
    {
        MPI_Recv(&(data->colNumber), 1, MPI_INT, 0, COL_NUM, MPI_COMM_WORLD, &status );
        MPI_Recv(&(data->rowNumber), 1, MPI_INT, 0, ROW_NUM, MPI_COMM_WORLD, &status );

        data->imageIntensity = allocateIntMemory(data->rowNumber, data->colNumber);
        data->tmpImageIntensity = allocateIntMemory(data->rowNumber, data->colNumber);

        MPI_Recv(&(data->imageIntensity[0][0]), data->rowNumber * data->colNumber, MPI_INT, 0, IMG_DATA, MPI_COMM_WORLD, &status );
    }

    computeSmoothing(data, mpiProcessId, processesNumber);

    if (mpiProcessId == 0)
    {
        unsigned int parallelBlockSize = (unsigned int) (data->rowNumber / processesNumber);

        for (int currentProcess = 1; currentProcess < processesNumber - 1; currentProcess++)
        {
            MPI_Recv(&(data->imageIntensity[parallelBlockSize * currentProcess][0]), data->colNumber * parallelBlockSize, MPI_INT, currentProcess, FINAL, MPI_COMM_WORLD, &status );
        }

        if (processesNumber > 1)
        {
            int startIndex = parallelBlockSize * (processesNumber - 1);
            int endIndex = data->rowNumber - 1;

            MPI_Recv(&(data->imageIntensity[startIndex][0]), data->colNumber * (endIndex - startIndex + 1), MPI_INT, processesNumber - 1, FINAL, MPI_COMM_WORLD, &status );
        }

        saveResult(data);
    } else
    {
        IterationsBounds iterationsBoundsTmp = getBounds(data, mpiProcessId, processesNumber);
        MPI_Send( &(data->imageIntensity[iterationsBoundsTmp.startIteration][0]), data->colNumber * (iterationsBoundsTmp.endIteration - iterationsBoundsTmp.startIteration + 1), MPI_INT, 0, FINAL, MPI_COMM_WORLD );
    }

    freeIntArray(data->imageIntensity);
    freeIntArray(data->tmpImageIntensity);

    free(data);
}

int main(int argc, char* argv[])
{
    const char* fileName = "C:/Users/dmitr/CLionProjects/lab3mpi/apollonian.pgm";

    int mpiProcessId, processesNumber;

    MPI_Init(&argc, &argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpiProcessId);
    MPI_Comm_size(MPI_COMM_WORLD, &processesNumber);

    clock_t begin = clock();

    run(fileName, mpiProcessId, processesNumber);

    clock_t end = clock();

    if (mpiProcessId == 0)
    {
        printf("time: %f", (float)(end - begin) / CLOCKS_PER_SEC);
    }

    MPI_Finalize();

    return 0;
}
