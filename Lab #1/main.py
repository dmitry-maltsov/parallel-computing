import configparser
from importlib.machinery import SourceFileLoader

from gwo import gwo


def run():

    config = configparser.ConfigParser()
    config.read('.conf')

    problem = SourceFileLoader('abc', config['problem']['fit_function']).load_module()

    solution = gwo(
        fitness=problem.fitness,
        max_iter=int(config['hyperparameters']['max_iter']),
        n=int(config['hyperparameters']['wolves_numb']),
        dim=int(config['problem parameters']['dim']),
        minx=float(config['problem parameters']['lower_bound']),
        maxx=float(config['problem parameters']['upper_bound']),
        threads=int(config['multithreading']['threads_numb'])
    )

    print(solution)
