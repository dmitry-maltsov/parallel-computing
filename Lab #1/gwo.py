import random
import copy

from multiprocessing.dummy import Pool as ThreadPool


class Wolf:
    def __init__(self, fitness, dim, minx, maxx, seed):
        self.rnd = random.Random(seed)
        self.position = [0.0 for i in range(dim)]

        for i in range(dim):
            self.position[i] = ((maxx - minx) * self.rnd.random() + minx)

        self.fit_func = fitness

        self.pos_new = []
        self.fit_new = 0
        self.fitness = 0


def wolf_fitness(wolf):
    wolf.fitness = wolf.fit_func(wolf.position)


def wolf_fitness_new(wolf):
    wolf.fit_new = wolf.fit_func(wolf.pos_new)


def gwo(fitness, max_iter, n, dim, minx, maxx, threads):

    rnd = random.Random()

    population = [Wolf(fitness, dim, minx, maxx, i) for i in range(n)]

    pool = ThreadPool(threads)

    pool.map(wolf_fitness, population)

    population = sorted(population, key=lambda temp: temp.fitness)

    alpha_wolf, beta_wolf, gamma_wolf = copy.copy(population[: 3])

    Iter = 0
    while Iter < max_iter:

        if Iter % 10 == 0:
            print(alpha_wolf.fitness)

        a = 2 * (1 - Iter / max_iter)

        for i in range(n):
            A1, A2, A3 = a * (2 * rnd.random() - 1), a * (
                    2 * rnd.random() - 1), a * (2 * rnd.random() - 1)
            C1, C2, C3 = 2 * rnd.random(), 2 * rnd.random(), 2 * rnd.random()

            X1 = [0.0 for i in range(dim)]
            X2 = [0.0 for i in range(dim)]
            X3 = [0.0 for i in range(dim)]
            Xnew = [0.0 for i in range(dim)]
            for j in range(dim):
                X1[j] = alpha_wolf.position[j] - A1 * abs(
                    C1 * alpha_wolf.position[j] - population[i].position[j])
                X2[j] = beta_wolf.position[j] - A2 * abs(
                    C2 * beta_wolf.position[j] - population[i].position[j])
                X3[j] = gamma_wolf.position[j] - A3 * abs(
                    C3 * gamma_wolf.position[j] - population[i].position[j])
                Xnew[j] += X1[j] + X2[j] + X3[j]

            for j in range(dim):
                Xnew[j] /= 3.0

            population[i].pos_new = Xnew

        pool.map(wolf_fitness_new, population)

        for i in range(n):
            if population[i].fit_new < population[i].fitness:
                population[i].position = population[i].pos_new
                population[i].fitness = population[i].fit_new

        population = sorted(population, key=lambda temp: temp.fitness)

        alpha_wolf, beta_wolf, gamma_wolf = copy.copy(population[: 3])

        Iter += 1

    return alpha_wolf.position
