#include <math.h>
#include <mpi.h>
#include <time.h>
#include <stdio.h>
#include <malloc.h>


#define SEND_PREV 4
#define RECEIVE_NEXT 4
#define IMG_DATA 0
#define DIST_DATA 1
#define ROW_NUM 2
#define COL_NUM 3
#define ITERATION_STATUS 6
#define FINAL 7
#define SEND_NEXT 5
#define RECEIVE_PREV 5

const int SRC_BRIGHT = 200;

MPI_Status status;

typedef struct IterationsBound
{
    int startIteration;
    int endIteration;
} IterationsBounds;


typedef struct ImageWithDistancesData
{
    int rowNumber;
    int colNumber;
    int maxGrayInImage;
    int **imageIntensity;
    float **distances;
} ImageWithDistancesData;


int ** allocateIntMemory(int row, int col)
{
    int * arrayDataMemory = (int *)malloc(row * col * sizeof(int));
    int ** returnedArray = (int **) malloc(row * sizeof(int *));

    for (int i = 0; i < row; i++){
        returnedArray[i] = &(arrayDataMemory[col * i]);
    }

    return returnedArray;
}

float ** allocateFloatMemory(int row, int col)
{
    float * arrayDataMemory = (float *)malloc(row * col * sizeof(float));
    float ** returnedArray = (float **) malloc(row * sizeof(float *));

    for (int i = 0; i < row; i++){
        returnedArray[i] = &(arrayDataMemory[col * i]);
    }

    return returnedArray;
}

void freeIntArray(int **array)
{
    free(array[0]);
    free(array);
}

void freeFloatArrayMemory(float **array)
{
    free(array[0]);
    free(array);
}

ImageWithDistancesData* readImageData(const char* fileName, ImageWithDistancesData* data)
{
    FILE* pgmImage;
    char pgmVersion[3];

    pgmImage = fopen(fileName, "rb");

    if (pgmImage == NULL)
    {
        printf("Can't open file");
        exit(1);
    }

    fgets(pgmVersion, sizeof(pgmVersion), pgmImage);

    fscanf(pgmImage, "%d", &(data->colNumber));
    fscanf(pgmImage, "%d", &(data->rowNumber));
    fscanf(pgmImage, "%d", &(data->maxGrayInImage));

    data->imageIntensity = allocateIntMemory(data->rowNumber, data->colNumber);

    for (int i = 0; i < data->rowNumber; i++)
    {
        for (int j = 0; j < data->colNumber; j++)
        {
            fscanf(pgmImage, "%d", &((data->imageIntensity)[i][j]));
        }
    }

    fclose(pgmImage);
    return data;
}

int getMaxFor2DArray(const float ** values, int rowSize, int colSize)
{
    float maxValue = 0;

    for (int i = 1; i < rowSize; i++)
    {
        for (int j = 1; j < colSize; j++)
        {
            if (values[i][j] > maxValue)
            {
                maxValue = values[i][j];
            }
        }
    }

    return (int) maxValue;
}

void saveResult(ImageWithDistancesData* data)
{
    int tmp;

    FILE* resImg;
    resImg = fopen("result.pgm", "wb");

    fprintf(resImg, "P2\n");
    fprintf(resImg, "%d %d\n", data->colNumber, data->rowNumber);
    fprintf(resImg, "%d\n", data->maxGrayInImage);

    int maxDistance = getMaxFor2DArray((const float **) data->distances, data->rowNumber, data->colNumber);

    float greyScale = (float) data->maxGrayInImage / (float) maxDistance;

    for (int i = 0; i < data->rowNumber; i++)
    {
        for (int j = 0; j < data->colNumber; j++)
        {
            tmp = (int) (data->distances[i][j] * greyScale);
            fprintf(resImg, "%d ", tmp);
        }
        fprintf(resImg, "\n");
    }

    fclose(resImg);
}

float ** getInitDistanceData(ImageWithDistancesData* data)
{
    data->distances = allocateFloatMemory(data->rowNumber, data->colNumber);
    int srcNum = 0;

    for (int i = 0; i < data->rowNumber; i++)
    {
        for (int j = 0; j < data->colNumber; j++)
        {
            data->distances[i][j] = 1;

            if (data->imageIntensity[i][j] > SRC_BRIGHT)
            {
                data->distances[i][j] = 0;
                srcNum += 1;
            }
        }
    }

    if (srcNum == 0) {
        data->distances[0][0] = 0;
    }

    return data->distances;
}

float getMin(const float* values, int size)
{
    float minValue = INFINITY;

    for (int i = 0; i < size; i++)
    {
        if (values[i] < minValue && values[i] != 1)
        {
            minValue = values[i];
        }
    }

    return minValue;
}

IterationsBounds getBounds(ImageWithDistancesData* data, unsigned int mpiProcessId, unsigned int processesNumber)
{
    unsigned int parallelBlockSize = (unsigned int) (data->rowNumber / processesNumber);

    IterationsBounds iterationsBounds;
    iterationsBounds.startIteration = parallelBlockSize * mpiProcessId - 1;
    iterationsBounds.endIteration = iterationsBounds.startIteration + parallelBlockSize;

    if (mpiProcessId == 0)
    {
        iterationsBounds.startIteration = 0;
    }

    if (mpiProcessId + 1 == processesNumber)
    {
        iterationsBounds.endIteration = data->rowNumber - 1;
    }

    return iterationsBounds;
}

float ** computeDown(ImageWithDistancesData* data, int* solutionChangedFlag, int mpiProcessId, int processesNumber)
{
    float values[5];
    IterationsBounds iterationsBounds = getBounds(data, mpiProcessId, processesNumber);

    for (int i = iterationsBounds.startIteration + 1; i < iterationsBounds.endIteration; i++) {
        for (int j = 1; j < data->colNumber - 1; j++) {

            float intensity = (float)(data->imageIntensity[i][j]);

            values[0] = data->distances[i - 1][j] + 1 * intensity;
            values[1] = data->distances[i][j - 1] + 1 * intensity;
            values[2] = data->distances[i - 1][j - 1] + 2 * intensity;
            values[3] = data->distances[i - 1][j + 1] + 2 * intensity;
            values[4] = data->distances[i][j];

            float newDist = getMin(values, 5);

            if (newDist != data->distances[i][j]) {
                data->distances[i][j] = newDist;
                *solutionChangedFlag = 1;
            }
        }
    }

    return data->distances;
}

float ** computeUpper(ImageWithDistancesData* data, int* solutionChangedFlag, int mpiProcessId, int processesNumber)
{
    float values[5];
    IterationsBounds iterationsBounds = getBounds(data, mpiProcessId, processesNumber);

    for (int i = iterationsBounds.endIteration - 1; i > iterationsBounds.startIteration; i--) {
        for (int j = data->colNumber - 1; j >= 1; j--) {
            float intensity = (float)(data->imageIntensity[i][j]);

            values[0] = data->distances[i + 1][j] + 1 * intensity;
            values[1] = data->distances[i][j + 1] + 1 * intensity;
            values[2] = data->distances[i + 1][j + 1] + 2 * intensity;
            values[3] = data->distances[i + 1][j - 1] + 2 * intensity;
            values[4] = data->distances[i][j];

            float newDist = getMin(values, 5);
            if (newDist != data->distances[i][j]) {
                data->distances[i][j] = newDist;
                *solutionChangedFlag = 1;
            }
        }
    }

    return data->distances;
}

float** computeMetric(ImageWithDistancesData* data, int mpiProcessId, int processesNumber, int* iterationsNumber)
{
    int solutionChangedFlag = 1;
    unsigned int maxIter = 1000;
    unsigned int currIter = 0;
    unsigned int parallelBlockSize = data->rowNumber / processesNumber;

    IterationsBounds iterationsBounds = getBounds(data, mpiProcessId, processesNumber);

    while (currIter < maxIter)
    {
        data->distances = computeDown(data, &solutionChangedFlag, mpiProcessId, processesNumber);
        data->distances = computeUpper(data, &solutionChangedFlag, mpiProcessId, processesNumber);

        if (mpiProcessId == 0)
        {
            int* solutionChangedFlagForProcess = (int*) malloc (sizeof(int) * (processesNumber - 1));

            for (int currProcessNum = 1; currProcessNum < processesNumber; currProcessNum++)
            {
                MPI_Recv(&(solutionChangedFlagForProcess[currProcessNum - 1]), 1, MPI_INT, currProcessNum, ITERATION_STATUS, MPI_COMM_WORLD, &status );
            }

            for (int currProcessNum = 1; currProcessNum < processesNumber; currProcessNum++)
            {
                if (solutionChangedFlagForProcess[currProcessNum - 1]){
                    solutionChangedFlag = 1;
                    break;
                }
            }
            for (int currProcessNum = 1; currProcessNum < processesNumber; currProcessNum++)
            {
                MPI_Send(&solutionChangedFlag, 1, MPI_INT, currProcessNum, ITERATION_STATUS, MPI_COMM_WORLD );
            }
        } else
        {
            MPI_Send(&solutionChangedFlag, 1, MPI_INT, 0, ITERATION_STATUS, MPI_COMM_WORLD );
            MPI_Recv(&solutionChangedFlag, 1, MPI_INT, 0, ITERATION_STATUS, MPI_COMM_WORLD, &status );
        }

        if (!solutionChangedFlag)
        {
            break;
        }

        if (mpiProcessId % 2)
        {
            if (mpiProcessId != 0)
            {
                MPI_Send(&(data->distances[iterationsBounds.startIteration + 1][0]), data->colNumber, MPI_FLOAT, mpiProcessId - 1, SEND_PREV, MPI_COMM_WORLD );
            }
            if (mpiProcessId + 1 != processesNumber)
            {
                MPI_Send(&(data->distances[iterationsBounds.endIteration - 1][0]), data->colNumber, MPI_FLOAT, mpiProcessId + 1, SEND_NEXT, MPI_COMM_WORLD );
            }
            if (mpiProcessId + 1 != processesNumber)
            {
                MPI_Recv(&(data->distances[iterationsBounds.endIteration][0]), data->colNumber, MPI_FLOAT, mpiProcessId + 1, RECEIVE_NEXT, MPI_COMM_WORLD, &status );
            }
            if (mpiProcessId != 0)
            {
                MPI_Recv(&(data->distances[iterationsBounds.startIteration][0]), data->colNumber, MPI_FLOAT, mpiProcessId - 1, RECEIVE_PREV, MPI_COMM_WORLD, &status );
            }
        }
        else
        {
            if (mpiProcessId + 1 != processesNumber)
            {
                MPI_Recv(&(data->distances[iterationsBounds.endIteration][0]), data->colNumber, MPI_FLOAT, mpiProcessId + 1, RECEIVE_NEXT, MPI_COMM_WORLD, &status );
            }
            if (mpiProcessId != 0)
            {
                MPI_Recv(&(data->distances[iterationsBounds.startIteration][0]), data->colNumber, MPI_FLOAT, mpiProcessId - 1, RECEIVE_PREV, MPI_COMM_WORLD, &status );
            }

            if (mpiProcessId != 0)
            {
                MPI_Send(&(data->distances[iterationsBounds.startIteration + 1][0]), data->colNumber, MPI_FLOAT, mpiProcessId - 1, SEND_PREV, MPI_COMM_WORLD );
            }
            if (mpiProcessId + 1 != processesNumber)
            {
                MPI_Send(&(data->distances[iterationsBounds.endIteration - 1][0]), data->colNumber, MPI_FLOAT, mpiProcessId + 1, SEND_NEXT, MPI_COMM_WORLD );
            }
        }

        solutionChangedFlag = 0;
        currIter += 1;
    }

    if (mpiProcessId == 0)
    {
        for (int currProcessNum = 1; currProcessNum < processesNumber - 1; currProcessNum++)
        {
            MPI_Recv(&(data->distances[parallelBlockSize * currProcessNum][0]), data->colNumber * parallelBlockSize, MPI_FLOAT, currProcessNum, FINAL, MPI_COMM_WORLD, &status );
        }

        if (processesNumber != 1)
        {
            IterationsBounds iterationsBoundsNoneFirstProcess;
            iterationsBoundsNoneFirstProcess.startIteration = parallelBlockSize * (processesNumber - 1) - 1;
            iterationsBoundsNoneFirstProcess.endIteration = data->rowNumber - 1;

            MPI_Recv(&(data->distances[iterationsBoundsNoneFirstProcess.startIteration][0]), data->colNumber * (iterationsBoundsNoneFirstProcess.endIteration - iterationsBoundsNoneFirstProcess.startIteration - 1), MPI_FLOAT, processesNumber - 1, FINAL, MPI_COMM_WORLD, &status );
        }
    } else
    {
        MPI_Send(&(data->distances[iterationsBounds.startIteration + 1][0]), data->colNumber * (iterationsBounds.endIteration - iterationsBounds.startIteration - 1), MPI_FLOAT, 0, FINAL, MPI_COMM_WORLD );
    }

    *iterationsNumber = currIter;

    return data->distances;
}


int run(const char* filename, int mpiProcessId, int processesNumber)
{
    ImageWithDistancesData * data = (ImageWithDistancesData*)malloc(sizeof(ImageWithDistancesData));

    int iterCounter = 0;

    if (mpiProcessId == 0)
    {
        data = readImageData(filename, data);

        for (int currProcessNum = 1; currProcessNum < processesNumber; currProcessNum++)
        {
            MPI_Send(&(data->colNumber), 1, MPI_INT, currProcessNum, COL_NUM, MPI_COMM_WORLD );
            MPI_Send(&(data->rowNumber), 1, MPI_INT, currProcessNum, ROW_NUM, MPI_COMM_WORLD );
        }

        data->distances = getInitDistanceData(data);

        for (int currProcessNum = 1; currProcessNum < processesNumber; currProcessNum++)
        {
            MPI_Send(&(data->imageIntensity[0][0]), data->rowNumber * data->colNumber, MPI_INT, currProcessNum, IMG_DATA, MPI_COMM_WORLD );
            MPI_Send(&(data->distances[0][0]), data->rowNumber * data->colNumber, MPI_FLOAT, currProcessNum, DIST_DATA, MPI_COMM_WORLD );
        }
    }
    else
    {
        MPI_Recv(&(data->colNumber), 1, MPI_INT, 0, COL_NUM, MPI_COMM_WORLD, &status );
        MPI_Recv(&(data->rowNumber), 1, MPI_INT, 0, ROW_NUM, MPI_COMM_WORLD, &status );

        data->imageIntensity = allocateIntMemory(data->rowNumber, data->colNumber);
        data->distances = allocateFloatMemory(data->rowNumber, data->colNumber);

        MPI_Recv(&(data->imageIntensity[0][0]), data->rowNumber * data->colNumber, MPI_INT, 0, IMG_DATA, MPI_COMM_WORLD, &status );
        MPI_Recv(&(data->distances[0][0]), data->rowNumber * data->colNumber, MPI_FLOAT, 0, DIST_DATA, MPI_COMM_WORLD, &status );
    }

    data->distances = computeMetric(data, mpiProcessId, processesNumber, &iterCounter);

    if (mpiProcessId == 0)
    {
        saveResult(data);
    }

    freeIntArray(data->imageIntensity);
    freeFloatArrayMemory(data->distances);
    free(data);

    return iterCounter;
}

int main(int argc, char* argv[])
{
    const char* fileName = "C:/Users/dmitr/CLionProjects/lab2mpi/apollonian.pgm";

    int mpiProcessId, processesNumber;

    unsigned int iterCounter;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &mpiProcessId);
    MPI_Comm_size(MPI_COMM_WORLD, &processesNumber);

    clock_t begin = clock();

    iterCounter = run(fileName, mpiProcessId, processesNumber);

    clock_t end = clock();

    if (mpiProcessId == 0)
    {
        printf("time: %f , iters: %d", (double)(end - begin) / CLOCKS_PER_SEC, iterCounter);
    }

    MPI_Finalize();

    return 0;
}
