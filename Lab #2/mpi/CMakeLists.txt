cmake_minimum_required(VERSION 3.5)

project(lab2mpi LANGUAGES C)

add_executable(lab2mpi main.c)

find_package(MPI) #make it REQUIRED, if you want
include_directories(SYSTEM "C:/Program Files (x86)/Microsoft SDKs/MPI/Include")
target_link_libraries(lab2mpi "C:/Program Files (x86)/Microsoft SDKs/MPI/Lib/x64/msmpi.lib")

install(TARGETS lab2mpi
        LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR})

set (CMAKE_CXX_STANDARD 11)