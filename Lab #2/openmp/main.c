#include <stdio.h>
#include <malloc.h>
#include <math.h>
#include <time.h>
#include <omp.h>

#define THREAD_NUM 4

const int SRC_BRIGHT = 200;

typedef struct ImageWithDistancesData {
    int rowNumber;
    int colNumber;
    int maxGrayInImage;
    int **imageIntensity;
    float **distances;
} ImageWithDistancesData;

typedef struct IterationsBound {
    int startIteration;
    int endIteration;
} IterationsBounds;

int ** allocateIntMemory(int row, int col)
{
    int * arrayDataMemory = (int *)malloc(row * col * sizeof(int));
    int ** returnedArray = (int **) malloc(row * sizeof(int *));

    for (int i = 0; i < row; i++){
        returnedArray[i] = &(arrayDataMemory[col * i]);
    }

    return returnedArray;
}

float ** allocateFloatMemory(int row, int col)
{
    float * arrayDataMemory = (float *)malloc(row * col * sizeof(float));
    float ** returnedArray = (float **) malloc(row * sizeof(float *));

    for (int i = 0; i < row; i++){
        returnedArray[i] = &(arrayDataMemory[col * i]);
    }

    return returnedArray;
}

void freeIntArray(int **array)
{
    free(array[0]);
    free(array);
}

void freeFloatArrayMemory(float **array)
{
    free(array[0]);
    free(array);
}

ImageWithDistancesData* readImageData(const char* fileName, ImageWithDistancesData* data)
{
    FILE* pgmImage;
    char pgmVersion[3];

    pgmImage = fopen(fileName, "rb");

    if (pgmImage == NULL)
    {
        printf("Can't open file");
        exit(1);
    }

    fgets(pgmVersion, sizeof(pgmVersion), pgmImage);

    fscanf(pgmImage, "%d", &(data->colNumber));
    fscanf(pgmImage, "%d", &(data->rowNumber));
    fscanf(pgmImage, "%d", &(data->maxGrayInImage));

    data->imageIntensity = allocateIntMemory(data->rowNumber, data->colNumber);

    for (int i = 0; i < data->rowNumber; i++)
    {
        for (int j = 0; j < data->colNumber; j++)
        {
            fscanf(pgmImage, "%d", &((data->imageIntensity)[i][j]));
        }
    }

    fclose(pgmImage);
    return data;
}

int getMaxFor2DArray(const float ** values, int rowSize, int colSize)
{
    float maxValue = 0;

    for (int i = 1; i < rowSize; i++)
    {
        for (int j = 1; j < colSize; j++)
        {
            if (values[i][j] > maxValue)
            {
                maxValue = values[i][j];
            }
        }
    }

    return (int) maxValue;
}

void saveResult(ImageWithDistancesData* data)
{
    int tmp;

    FILE* resImg;
    resImg = fopen("result.pgm", "wb");

    fprintf(resImg, "P2\n");
    fprintf(resImg, "%d %d\n", data->colNumber, data->rowNumber);
    fprintf(resImg, "%d\n", data->maxGrayInImage);

    int maxDistance = getMaxFor2DArray((const float **) data->distances, data->rowNumber, data->colNumber);

    float greyScale = (float) data->maxGrayInImage / (float) maxDistance;

    for (int i = 0; i < data->rowNumber; i++)
    {
        for (int j = 0; j < data->colNumber; j++)
        {
            tmp = (int) (data->distances[i][j] * greyScale);
            fprintf(resImg, "%d ", tmp);
        }
        fprintf(resImg, "\n");
    }

    fclose(resImg);
}

float ** getInitDistanceData(ImageWithDistancesData* data)
{
    data->distances = allocateFloatMemory(data->rowNumber, data->colNumber);
    int srcNum = 0;

    for (int i = 0; i < data->rowNumber; i++)
    {
        for (int j = 0; j < data->colNumber; j++)
        {
            data->distances[i][j] = 1;

            if (data->imageIntensity[i][j] > SRC_BRIGHT){
                data->distances[i][j] = 0;
                srcNum += 1;
            }
        }
    }

    if (srcNum == 0)
    {
        data->distances[0][0] = 0;
    }

    return data->distances;
}

float getMin(const float* values, int size)
{
    float minValue = INFINITY;

    for (int i = 0; i < size; i++)
    {
        if (values[i] < minValue && values[i] != 1)
        {
            minValue = values[i];
        }
    }

    return minValue;
}

IterationsBounds getBounds(ImageWithDistancesData* data, int row)
{
    unsigned int parallelBlockSize = (unsigned int) (data->rowNumber / omp_get_num_threads());

    IterationsBounds iterationsBounds;
    iterationsBounds.startIteration = parallelBlockSize * omp_get_thread_num() - 1;
    iterationsBounds.endIteration = iterationsBounds.startIteration + parallelBlockSize;

    if (omp_get_thread_num() == 0)
    {
        iterationsBounds.startIteration = 0;
    }

    if (omp_get_thread_num() + 1 == omp_get_num_threads())
    {
        iterationsBounds.endIteration = row - 1;
    }

    return iterationsBounds;
}

float ** computeDown(ImageWithDistancesData* data, int* solutionChangedFlag, int row, int col)
{
    float values[5];
    IterationsBounds iterationsBounds = getBounds(data, row);

    for (int i = iterationsBounds.startIteration + 1; i < iterationsBounds.endIteration; i++)
    {
        for (int j = 1; j < col - 1; j++)
        {

            float intensity = (float)(data->imageIntensity[i][j]);

            values[0] = data->distances[i - 1][j] + 1 * intensity;
            values[1] = data->distances[i][j - 1] + 1 * intensity;
            values[2] = data->distances[i - 1][j - 1] + 2 * intensity;
            values[3] = data->distances[i - 1][j + 1] + 2 * intensity;
            values[4] = data->distances[i][j];

            float newDist = getMin(values, 5);

            if (newDist != data->distances[i][j])
            {
#pragma omp critical
                {
                    data->distances[i][j] = newDist;
                    *solutionChangedFlag = 1;
                }
            }
        }
    }

    return data->distances;
}

float ** computeUpper(ImageWithDistancesData* data, int* solutionChangedFlag, int row, int col)
{
    float values[5];
    IterationsBounds iterationsBounds = getBounds(data, row);

    for (int i = iterationsBounds.endIteration - 1; i > iterationsBounds.startIteration; i--)
    {
        for (int j = col - 1; j >= 1; j--)
        {

            float intensity = (float)(data->imageIntensity[i][j]);

            values[0] = data->distances[i + 1][j] + 1 * intensity;
            values[1] = data->distances[i][j + 1] + 1 * intensity;
            values[2] = data->distances[i + 1][j + 1] + 2 * intensity;
            values[3] = data->distances[i + 1][j - 1] + 2 * intensity;
            values[4] = data->distances[i][j];

            float newDist = getMin(values, 5);
            if (newDist != data->distances[i][j])
            {
#pragma omp critical
                {
                    data->distances[i][j] = newDist;
                    *solutionChangedFlag = 1;
                }
            }
        }
    }

    return data->distances;
}

float** computeMetric(ImageWithDistancesData* data, int* iterationsNumber)
{
    int solutionChangedFlag = 1;
    unsigned int maxIter = 10000;
    unsigned int currIter = 0;

    while (currIter < maxIter)
    {
#pragma omp parallel num_threads(THREAD_NUM)
        {
            int row, col;
#pragma omp critical
            {
                row = data->rowNumber;
                col = data->colNumber;
            }

            computeDown(data, &solutionChangedFlag, row, col);
            computeUpper(data, &solutionChangedFlag, row, col);
        }

        if (!solutionChangedFlag)
        {
            break;
        }

        solutionChangedFlag = 0;
        currIter += 1;
    }

    *iterationsNumber = currIter;

    return data->distances;
}


int run(const char* filename)
{
    ImageWithDistancesData * data = (ImageWithDistancesData*)malloc(sizeof(ImageWithDistancesData));
    int iterCounter = 0;

    data = readImageData(filename, data);
    data->distances = getInitDistanceData(data);
    data->distances = computeMetric(data, &iterCounter);

    saveResult(data);

    freeIntArray(data->imageIntensity);
    freeFloatArrayMemory(data->distances);
    free(data);

    return iterCounter;
}

int main(int argc, char* argv[])
{
    const char* fileName = "C:/Users/dmitr/CLionProjects/lab2omp/apollonian.pgm";
    unsigned int iterCounter;

    clock_t begin = clock();

    iterCounter = run(fileName);

    clock_t end = clock();

    printf("time: %f , iters: %d", (double)(end - begin) / CLOCKS_PER_SEC, iterCounter);

    return 0;
}
